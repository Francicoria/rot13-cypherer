#include <stdio.h>

// 'ALPHABET_LENGHT' will be rounded up by defect if it's not even, try putting 27 instead of 26.
#define ALPHABET_LENGHT 26
#define CYPHER_OFFSET   ALPHABET_LENGHT/2 

int main(int argc, char ** argv) {
	if (argc < 2) {
		fprintf(stderr, "One argument is expected.\n");
		fprintf(stderr, "Usage: %s \"text to encode\"\n", argv[0]);
		return 1;
	} else if (argc > 2) {
		fprintf(stderr, "If you want to cypher more then one word,\nput aphostrophes(\") at the start and end of the text.\n");
		fprintf(stderr, "Usage: %s \"text to encode\"\n", argv[0]);
		return 1;
	}

	char * input = argv[1];

	printf("\"%s\" -> \"", input);
	for (int i = 0; input[i] != '\0'; ++i) {
		if (input[i] < 'A' || input[i] > 'z' || (input[i] > 'Z' && input[i] < 'a')) {
			putc(input[i], stdout);
		} else {
			int offset = 0;
			if (input[i] <= 'Z') {
				offset = 32;
			}
			putc(
				(input[i] - (!offset ? 97 : 65) < CYPHER_OFFSET ?
				    input[i] + CYPHER_OFFSET :
				    input[i] - CYPHER_OFFSET),
				stdout);
		}
	}
	puts("\"");
	return 0;
}
